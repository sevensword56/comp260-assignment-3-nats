﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(Rigidbody))]
public class EnemyMove : MonoBehaviour {

    public float xSpeed = 0.05f, timedDestroy = 5.0f, maxY, ySpeed;
    public ParticleSystem explosionPrefab;

    private Rigidbody rigidbody;
    private float yMovement = 0.0f;
    private bool moveUp = true;
    
    // Use this for initialization
    void Start ()
    {
        rigidbody = GetComponent<Rigidbody>();
        Destroy(gameObject, timedDestroy);
        

    }

    // Physics Calculations
    void FixedUpdate()
    {
        Vector3 pos = rigidbody.position;


        pos.x -= xSpeed;

        if (yMovement < maxY && moveUp)
        {
            pos.y += ySpeed;
            yMovement += ySpeed;
        }
        else if (yMovement < maxY && !moveUp)
        {
            pos.y -= ySpeed;
            yMovement += ySpeed;
        }

        if (yMovement >= maxY && moveUp)
        {
            moveUp = false;
            yMovement = 0;
        }
        else if (yMovement >= maxY && !moveUp)
        {
            moveUp = true;
            yMovement = 0;
        }

        rigidbody.position = pos;

    }

    void OnTriggerEnter(Collider collider)
    {
        Destroy(gameObject);
        Scorekeeper.Instance.LosePoints();
        Scorekeeper.Instance.LoseLife();
    }

    void OnDestroy()
    {
        ParticleSystem explosion = Instantiate(explosionPrefab);
        explosion.transform.position = transform.position;
    }
}
