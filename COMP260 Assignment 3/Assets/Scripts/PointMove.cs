﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PointMove : MonoBehaviour {

    public float xSpeed = 0.05f, timedDestroy = 5.0f, maxY, ySpeed;
    public float rotationSpeed;
   
    private Rigidbody rigidbody;
    private float yMovement = 0.0f, rotation;
    private bool moveUp = true;


    // Use this for initialization
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        Destroy(gameObject, timedDestroy);


    }

    // Physics Calculations
    void FixedUpdate()
    {
        Vector3 pos = rigidbody.position;


        rotation += rotationSpeed;
        pos.x -= xSpeed;

        if (yMovement < maxY && moveUp)
        {
            pos.y += ySpeed;
            yMovement += ySpeed;
        }
        else if (yMovement < maxY && !moveUp)
        {
            pos.y -= ySpeed;
            yMovement += ySpeed;
        }

        if (yMovement >= maxY && moveUp)
        {
            moveUp = false;
            yMovement = 0;
        }
        else if (yMovement >= maxY && !moveUp)
        {
            moveUp = true;
            yMovement = 0;
        }

        rigidbody.position = pos;
        rigidbody.MoveRotation(Quaternion.Euler(0,0,rotation));

    }

    void OnTriggerEnter(Collider collider)
    {
        Destroy(gameObject);
        Scorekeeper.Instance.ScorePoints();
    }
}
