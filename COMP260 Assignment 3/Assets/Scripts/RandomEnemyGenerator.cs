﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomEnemyGenerator : MonoBehaviour {

    public float minWait, maxWait, minY, maxY;
    public float difficultyRamp = 0.0f, minHardWait, maxSpeed;
    public EnemyMove enemyPrefab;

    private Transform spawner;
    private float timer = 0.0f, randTime, xSpeed, ySpeed;

    // Use this for initialization
    void Start()
    {
        spawner = GetComponent<Transform>();
        randTime = Random.Range(minWait, maxWait);
        xSpeed = enemyPrefab.xSpeed;
        ySpeed = enemyPrefab.ySpeed;

    }

    // Update is called once per frame
    void Update ()
    {
        if (Scorekeeper.Instance.IsStart())
        {
            if (maxWait > minHardWait)
                maxWait -= difficultyRamp;

            if (xSpeed < maxSpeed)
                xSpeed += difficultyRamp;
            if (ySpeed < maxSpeed)
                ySpeed += difficultyRamp;

            if (timer >= randTime)
            {
                createEnemy(xSpeed, ySpeed);
                timer = 0;
                randTime = Random.Range(minWait, maxWait);
            }
        }

        timer += 1 * Time.deltaTime;

    }

    void createEnemy(float xSpeed, float ySpeed)
    {
        EnemyMove enemy = Instantiate(enemyPrefab);
        Vector3 pos = Vector3.zero;
        Color color = Color.magenta;
        
        pos.y = Random.Range(minY, maxY);

        enemy.transform.position = spawner.position + pos;
        enemy.xSpeed = xSpeed;
        enemy.ySpeed = ySpeed;
    }
}
