﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMove : MonoBehaviour {

    public float speed = 1.0f;
    public float boost = 1.0f;
    private Rigidbody rigidbody;
    

	// Use this for initialization
	void Start () {

        rigidbody = GetComponent<Rigidbody>();

	}
	
	// Physics Calculations
	void FixedUpdate () {

        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        Vector3 velocity = rigidbody.velocity;

        velocity.z = 0;
        if ( (horizontal > 0 || horizontal < 0 || vertical < 0 || vertical > 0) && Input.GetButton("Jump") )
        {
            velocity.x = 0;
            velocity.x += (horizontal*boost) * speed;
            velocity.y = 0;
            velocity.y += (vertical*boost) * speed;
                      
            rigidbody.velocity = velocity;
        }
        if ((horizontal > 0 || horizontal < 0 || vertical < 0 || vertical > 0) && !Input.GetButton("Jump"))
        {
            velocity.x = 0;
            velocity.x += horizontal * speed;
            velocity.y = 0;
            velocity.y += vertical * speed;

            rigidbody.velocity = velocity;
        }


    }
}
