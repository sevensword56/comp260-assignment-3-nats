﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomLevelGenerator : MonoBehaviour {

    public float minWidth, maxWidth, minLength, maxLength, minWait, maxWait, minY, maxY, minRed, maxRed, minBlue, maxBlue;
    public float difficultyRamp = 0.0f, minHardWait, maxSpeed;
    public TerrainMover terrainPrefab;

    private Transform spawner;
    private float timer = 0.0f, randTime, speed;

	// Use this for initialization
	void Start ()
    {
        spawner = GetComponent<Transform>();
        randTime = Random.Range(minWait, maxWait);
        speed = terrainPrefab.speed;

	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Scorekeeper.Instance.IsStart())
        {
            if (maxWait > minHardWait)
                maxWait -= difficultyRamp;

            if (speed < maxSpeed)
                speed += difficultyRamp;

            if (timer >= randTime)
            {
                createBlock(speed);
                timer = 0;
                randTime = Random.Range(minWait, maxWait);
            }

        }
        timer += 1 * Time.deltaTime;
	}

    void createBlock(float speed)
    {
        TerrainMover block = Instantiate(terrainPrefab);
        Vector3 size = Vector3.zero;
        Vector3 pos = Vector3.zero;
        Color color = Color.magenta;
        Renderer rend = block.GetComponent<Renderer>();

        size.x = Random.Range(minLength, maxLength);
        size.y = Random.Range(minWidth, maxWidth);
        pos.y = Random.Range(minY, maxY);
        color.r = Random.Range(minRed, maxRed);
        color.b = Random.Range(minBlue, maxBlue);

        block.transform.localScale -= size;
        block.transform.position = spawner.position + pos;
        rend.material.color = color;
        block.speed = speed;
    }
}
