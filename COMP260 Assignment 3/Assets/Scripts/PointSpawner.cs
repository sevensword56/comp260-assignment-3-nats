﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointSpawner : MonoBehaviour {

    public float minWait, maxWait, minY, maxY;
    public float difficultyRamp = 0.0f, minHardWait, maxSpeed;
    public PointMove pointPrefab;

    private Transform spawner;
    private float timer = 0.0f, randTime, xSpeed, ySpeed;

    // Use this for initialization
    void Start()
    {
        spawner = GetComponent<Transform>();
        randTime = Random.Range(minWait, maxWait);
        xSpeed = pointPrefab.xSpeed;
        ySpeed = pointPrefab.ySpeed;

    }

    // Update is called once per frame
    void Update()
    {
        if (Scorekeeper.Instance.IsStart())
        {
            if (maxWait > minHardWait)
                maxWait -= difficultyRamp;

            if (xSpeed < maxSpeed)
                xSpeed += difficultyRamp;
            if (ySpeed < maxSpeed)
                ySpeed += difficultyRamp;

            if (timer >= randTime)
            {
                createPoint(xSpeed, ySpeed);
                timer = 0;
                randTime = Random.Range(minWait, maxWait);
            }
        }

        timer += 1 * Time.deltaTime;

    }

    void createPoint(float xSpeed, float ySpeed)
    {
        PointMove point = Instantiate(pointPrefab);
        Vector3 pos = Vector3.zero;
        Color color = Color.magenta;

        pos.y = Random.Range(minY, maxY);

        point.transform.position = spawner.position + pos;
        point.xSpeed = xSpeed;
        point.ySpeed = ySpeed;
    }
}
