﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Scorekeeper : MonoBehaviour {

    public float pointGain = 100, pointLoss = 300, lives = 3, constantPointGain = 1;
    public Text scoreText, gameOverText, livesText;
    public AudioClip explodeClip, coinClip;
    public float readTime = 20.0f;

    private bool isEndGame = false, isStart = false;
    private float score = 0, timer = 0;
    private int displayScore = 0;
    private AudioSource audio;

    static private Scorekeeper instance;

	// Use this for initialization
	void Start ()
    {
        if (instance == null)
            instance = this;
        else
            Debug.LogError("More than one Scorekeeper exists in the scene.");

        scoreText.text = "0";
        livesText.text = "Lives : " + lives.ToString();

        audio = GetComponent<AudioSource>();
    }

    void Update()
    {
        if(lives <= 0)
        {
            EndGame();
        }

        score += constantPointGain * Time.deltaTime;
        displayScore = Mathf.RoundToInt(score);
        scoreText.text = displayScore.ToString();

        if (timer >= readTime && !isStart)
        {
            isStart = true;
            gameOverText.text = null;
        }

        timer += 1 * Time.deltaTime;
    }

    public void ScorePoints()
    {
        score += pointGain;
        displayScore = Mathf.RoundToInt(score);
        scoreText.text = displayScore.ToString();
        audio.PlayOneShot(coinClip);
    }

    public void LosePoints()
    {
        score -= pointLoss;
        displayScore = Mathf.RoundToInt(score);
        scoreText.text = displayScore.ToString();
        audio.PlayOneShot(explodeClip);
    }

    public void LoseLife()
    {
        lives--;
        livesText.text = "Lives : " + lives.ToString();
    }

    public void EndGame()
    {
        Time.timeScale = 0;
        gameOverText.text = "GAME OVER \n" + displayScore.ToString();
        isEndGame = true;
    }

    public bool IsEndGame()
    {
        return isEndGame;
    }

    public float Timer()
    {
        return timer;
    }

    public bool IsStart()
    {
        return isStart;
    }

    static public Scorekeeper Instance
    {
        get{ return instance; }
    }
}
