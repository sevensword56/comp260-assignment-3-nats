﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class TerrainMover : MonoBehaviour {

    public float speed = 0.05f, timedDestroy = 5.0f;
    private Rigidbody rigidbody;

	// Use this for initialization
	void Start ()
    {
        rigidbody = GetComponent<Rigidbody>();
        Destroy(gameObject, timedDestroy);
	}
	
	// Physics Calculations
	void FixedUpdate () {
        Vector3 pos = rigidbody.position;

        pos.x -= speed;
        pos.z = 0;

        rigidbody.position = pos; 
		
	}

    
}
